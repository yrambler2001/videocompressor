﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace VideoCompressor {

	class Program {
		static void Main(string[] args) {
			//Func<int, string> sMonth = delegate (int ii) { return mon[--ii]; };
			Action<string> testDir = delegate (string s) { if (!Directory.Exists(s)) Directory.CreateDirectory(s); };
			Action<string> log = delegate (string s) { Console.WriteLine(s); };
			string doDir = "C:\\PA\\VideoConverter\\Files\\NeedConvert";
			string doneDir = "C:\\PA\\VideoConverter\\Files\\Converted";

			testDir("C:\\PA");
			testDir(doDir);
			testDir(doneDir);

			var vid = Directory.EnumerateFiles(doDir, "*.*", SearchOption.AllDirectories).ToList();
			log("CountVideo=" + vid.Count);
			vid.ForEach(x => {
				var nm = x.ToString().Split('\\').Last();
				var nmO = nm.Remove(nm.LastIndexOf('.')) + ".mp4";
				Console.WriteLine();
				ProcessStartInfo startInfo = new ProcessStartInfo("C:\\PA\\VideoConverter\\ffmpeg\\ffmpeg.exe", "-i "+doDir+"\\"+nm+" -c:v libx265 -crf 27 -preset veryfast -c:a aac -b:a 256k "+doneDir+"\\"+nmO) {
					WindowStyle = ProcessWindowStyle.Hidden,
					UseShellExecute = false,
					RedirectStandardOutput = true,
					RedirectStandardError=true,
					CreateNoWindow = true
				};
				Console.WriteLine(startInfo.Arguments);
				
				Process process = Process.Start(startInfo);
				process.ErrorDataReceived += (sender, e) => Console.Write(e.Data);
				process.OutputDataReceived += (sender, e) => Console.Write(e.Data);
				process.BeginOutputReadLine();
				process.BeginErrorReadLine();
				process.WaitForExit();
				// We may not have received all the events yet!
				Thread.Sleep(500);
				//string s = doneDir + "\\" + yr + "\\" + sMonth(Int32.Parse(mo));
			});

			Thread.Sleep(5000);

			int i = 1;


		}
	}
}


